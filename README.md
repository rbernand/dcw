# DCW
WIP

![Logo](docs/assets/dcw_logo.png)

A CLI to manage docker compose stack.

![coverage](https://gitlab.com/rbernand/dcw/badges/main/coverage.svg)
![pipeline](https://gitlab.com/rbernand/dcw/badges/main/pipeline.svg)
![latest_release](https://gitlab.com/rbernand/dcw/-/badges/release.svg)

## Installation

```
pip install .
```
For local development:
```
pip install -e .
```
## Usage

```
$ dcw -h
usage: dcw [-h] action ...

positional arguments:
  action
    add       AddExecute a command in all projects
    all       Execute a docker-compose command in all projects directory.
    config    Show DCW current configuration.
    go        Move to project's directory.
    in        Execute a docker-compose command in project directory.
    label     Manage label for a project.
    list      List available projects.
    rm        Remove a project from DCW registry.

options:
  -h, --help  show this help message and exit
```

## Example
```
$ dcw add tools ~/stacks/tools
New project 'tools' created at /home/rbernand/stacks/tools

$ dcw list
tools: /home/rbernand/stacks/tools

$ dcw in test ps
docker compose ps
NAME                IMAGE                    COMMAND                  SERVICE     CREATED      STATUS       PORTS
tools-logs-1        amir20/dozzle            "/dozzle"                logs        2 days ago   Up 2 hours   8080/tcp
tools-portainer-1   portainer/portainer-ce   "/portainer --http-e…"   portainer   2 days ago   Up 2 hours   8000/tcp, 9000/tcp, 9443/tcp
tools-proxy-1       jwilder/nginx-proxy      "/app/docker-entrypo…"   proxy       2 days ago   Up 2 hours   0.0.0.0:80->80/tcp, :::80->80/tcp
```

## Documentation
Full documentation is available [here](https://https://rbernand.gitlab.io/dcw/)
