import argparse

from .base_command import BaseCommand
import dcw.database as db


class RmCommand(BaseCommand):
    command_name = "rm"
    help = "Remove a project from DCW registry."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None:  # pragma: no cover
        subparser.add_argument("project_name")

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        project = db.get(args.project_name)
        db.remove(project)
        print(f"Project '{project.name}' deleted.")
