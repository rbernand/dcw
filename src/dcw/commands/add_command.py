import argparse
import pathlib

from .base_command import BaseCommand
import dcw.database as db
from dcw.models import Project


class AddCommand(BaseCommand):
    command_name = "add"
    help = "Add a new project."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None:  # pragma: no cover
        subparser.add_argument("project_name")
        subparser.add_argument("project_path", type=pathlib.Path)
        subparser.add_argument(
            "-l", "--labels", action="append", type=str, help="add a label"
        )
        subparser.add_argument(
            "-c", "--compose-file", action="append", help="add a compose file."
        )
        subparser.add_argument(
            "-f", "--force", action="store_true", help="Ignore directory validation."
        )

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        project = Project(
            name=args.project_name,
            path=args.project_path,
            labels=args.labels or set(),
            compose_files=args.compose_file or [],
        )
        if not args.force:
            project.check()
        r = db.create(project)
        print(f"New project '{r.name}' created at {r.path}")
