import argparse
import os
import subprocess  # nosec
import sys

from .base_command import BaseCommand
import dcw.database as db


class InCommand(BaseCommand):
    command_name = "in"
    help = "Execute a docker-compose command in project directory."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None:  # pragma: no cover
        subparser.add_argument("project_name")

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        project = db.get(args.project_name)
        os.chdir(project.path)
        command = project.prepare_command(rest)
        print("Executing: ", " ".join(command))
        ret = subprocess.run(command, shell=False)  # nosec
        sys.exit(ret.returncode)
