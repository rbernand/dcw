from abc import ABC, abstractmethod
import argparse
from typing import Type, Self


class BaseCommand(ABC):
    __execution_maping: dict[str, "BaseCommand"] = {}
    __COMMAND_KEY = "command"

    @property
    @abstractmethod
    def help(self) -> str: ...

    @property
    @abstractmethod
    def command_name(self) -> str: ...

    @staticmethod
    @abstractmethod
    def _setup_parser(parser: argparse.ArgumentParser) -> None: ...

    @abstractmethod
    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None: ...

    @classmethod
    def execute(cls, args: argparse.Namespace, rest: list[str]) -> None:
        cls.__execution_maping[getattr(args, cls.__COMMAND_KEY)]._execute(
            args, rest
        )  # pragma: no cover

    @classmethod
    def init(cls) -> argparse.ArgumentParser:
        parser = cls.setup_parser()
        return parser

    @classmethod
    def setup_parser(cls) -> argparse.ArgumentParser:
        parser = argparse.ArgumentParser()
        subparsers = parser.add_subparsers(
            required=True, dest=cls.__COMMAND_KEY, metavar="action"
        )
        for child in cls.__iter_childs():
            cls.__execution_maping[child.command_name] = child()  # type: ignore
            subparser = subparsers.add_parser(child.command_name, help=child.help)  # type: ignore
            child._setup_parser(subparser)
        return parser

    @classmethod
    def __iter_childs(cls) -> list[Type[Self]]:
        return list(cls.__subclasses__())
