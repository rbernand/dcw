import argparse

from .base_command import BaseCommand
import dcw.database as db


class ShowCommand(BaseCommand):
    command_name = "show"
    help = "Display informations about a project."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None:  # pragma: no cover
        subparser.add_argument("project_name")

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        project = db.get(args.project_name)
        project.print()
