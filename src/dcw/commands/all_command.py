import argparse
import subprocess  # nosec
import os

from .base_command import BaseCommand
import dcw.database as db
from dcw.config import Config


class AllCommand(BaseCommand):
    command_name = "all"
    help = "Execute a docker-compose command in all projects directory."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None:  # pragma: no cover
        subparser.add_argument(
            "-l", "--label", action="append", dest="labels", help="filter on labels"
        )

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        for project in db.get_all(labels=args.labels):
            os.chdir(project.path)
            command = Config.docker_compose_command + rest
            print(f"-> In project {project.name}:", " ".join(command))
            subprocess.run(command)  # nosec
