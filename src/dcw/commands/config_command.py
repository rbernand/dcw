import argparse

from .base_command import BaseCommand
from dcw.config import Config


class ConfigCommand(BaseCommand):
    command_name = "config"
    help = "Show DCW current configuration."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None: ...

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        print("db_path:", Config.db_path)
        print("docker_compose_command:", " ".join(Config.docker_compose_command))
