import argparse

from .base_command import BaseCommand
import dcw.database as db


class ListCommand(BaseCommand):
    command_name = "list"
    help = "List available projects."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None: ...

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        if projects := db.get_all():
            for project in projects:
                print(f"{project.name}: {project.path}")
        else:
            print("No projects.")
