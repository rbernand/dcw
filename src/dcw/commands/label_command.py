import argparse
import sys

from .base_command import BaseCommand
import dcw.database as db


class LabelCommand(BaseCommand):
    command_name = "label"
    help = "Manage label for a project."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None:  # pragma: no cover
        subparser.add_argument("project_name")
        actions = subparser.add_subparsers(
            metavar="label_action", help="label subaction", dest="label_action"
        )
        action_add_parser = actions.add_parser("add")
        action_add_parser.add_argument("labels", nargs="+")

        action_rm_parser = actions.add_parser("rm")
        action_rm_parser.add_argument("labels", nargs="+")

    @staticmethod
    def _add_labels(project_labels: set, labels: list[str]):
        for label in labels:
            project_labels.add(label)

    @staticmethod
    def _remove_labels(project_labels: set, labels: list[str]):
        for label in labels:
            try:
                project_labels.remove(label)
            except KeyError:
                print(f"Label '{label}' not found.", file=sys.stderr)

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        project = db.get(args.project_name)
        {
            "add": self._add_labels,
            "rm": self._remove_labels,
        }[args.label_action](project.labels, args.labels)
        project.print_labels()
