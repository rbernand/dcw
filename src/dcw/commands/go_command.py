import argparse
import os

from .base_command import BaseCommand
import dcw.database as db


class GoCommand(BaseCommand):
    command_name = "go"
    help = "Move to project's directory."

    @staticmethod
    def _setup_parser(subparser: argparse.ArgumentParser) -> None:  # pragma: no cover
        subparser.add_argument("project_name")

    def _execute(self, args: argparse.Namespace, rest: list[str]) -> None:
        project = db.get(args.project_name)
        os.chdir(project.path)
        print("Launching a subshell.")
        shell = os.environ.get("SHELL", "/bin/sh")
        os.execl(shell, shell)  # nosec
