import sys

from dcw.commands.base_command import BaseCommand
from dcw.errors import DCWException


def main() -> None:
    try:
        parser = BaseCommand.init()
        args, rest = parser.parse_known_args()
        BaseCommand.execute(args, rest)
    except DCWException as error:
        print(error.msg, file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":  # pragma: no cover
    main()
