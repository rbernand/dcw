from dataclasses import dataclass, field
from pathlib import Path
from typing import Any

from dcw.config import Config
from dcw.errors import NoComposeFile


DEFAULT_COMPOSE_FILES = [
    "compose.yml",
    "docker-compose.yml",
]


@dataclass
class Project:
    name: str
    path: Path
    compose_files: list[Path] = field(default_factory=list)
    labels: set = field(default_factory=set)

    _path: Path = field(init=False, repr=False)
    _compose_files: list[Path] = field(init=False, repr=False)
    _labels: set = field(init=False, repr=False)

    @property  # type: ignore[no-redef]
    def path(self) -> Path:
        return self._path

    @path.setter
    def path(self, value: Path | str) -> None:
        if isinstance(value, str):
            self._path = Path(value).absolute()
        elif isinstance(value, Path):
            self._path = value.absolute()
        else:
            raise TypeError

    @property  # type: ignore[no-redef]
    def compose_files(self) -> list[Path]:  # noqa: F811
        return self._compose_files

    @compose_files.setter  # type: ignore[no-redef]
    def compose_files(self, values: list[str] | list[Path]) -> None:
        compose_files = []
        values = [] if isinstance(values, property) else values
        for compose_file in values:
            if isinstance(compose_file, str):
                compose_files.append(Path(compose_file))
            elif isinstance(compose_file, Path):
                compose_files.append(compose_file)
        self._compose_files = compose_files

    @property  # type: ignore[no-redef]
    def labels(self):  # noqa: F811
        return self._labels

    @labels.setter
    def labels(self, values):
        values = [] if isinstance(values, property) else values
        self._labels = set(values)

    def dict(self) -> dict[str, Any]:
        return {
            "name": self.name,
            "path": self.path.expanduser().as_posix(),
            "compose_files": [cf.as_posix() for cf in self.compose_files],
            "labels": list(self.labels),
        }

    def _find_default_compose_file(self) -> Path:
        for default_compose_file in DEFAULT_COMPOSE_FILES:
            compose_file = self.path / default_compose_file
            if compose_file.exists():
                return compose_file
        else:
            raise NoComposeFile()

    def check(self) -> bool:
        self._find_default_compose_file()
        return True

    def print(self) -> None:
        print(f"Name: {self.name}")
        print(f"Path: {self.path}")

        print("Compose files:")
        for compose_file in self.compose_files:
            print(f"    - {compose_file}")
        self.print_labels()

    def print_labels(self) -> None:
        print("Labels:")
        for label in self.labels:
            print(f"    - {label}")

    def prepare_command(self, args):
        def _add_compose_files():
            for compose_file in self.compose_files:
                yield "-f"
                yield compose_file.as_posix()

        return Config.docker_compose_command + list(_add_compose_files()) + args


@dataclass
class Profile:
    name: str
    project: Project
    compose_files: list[Path] = field(default_factory=lambda: list())
