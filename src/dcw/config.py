import configparser
from pathlib import Path
from os import environ


DEFAULT_DB_PATH = "~/.config/dcw/dcw.db"
DEFAULT_DOCKER_COMPOSE_COMMAND = "docker compose"

ENV_DEFAULT_DB_PATH = "DCW_DB_PATH"
ENV_DEFAULT_DOCKER_COMPOSE_COMMAND = "DCW_DOCKER_COMPOSE_COMMAND"


CONFIG_FILES = [
    Path("~/.config/dcw/dcw.ini"),
    Path("~/.dcwrc"),
]


class DCWConfig:
    def __init__(self) -> None:
        self.parser = configparser.ConfigParser()
        self._db_path: Path
        for config_file in CONFIG_FILES:
            if self.parser.read(config_file.expanduser()):
                break
        self.db_path = environ.get(ENV_DEFAULT_DB_PATH) or str(
            self.parser["DEFAULT"].get("db_path", DEFAULT_DB_PATH)
        )  # type: ignore
        self.docker_compose_command = environ.get(
            ENV_DEFAULT_DOCKER_COMPOSE_COMMAND
        ) or self.parser["DEFAULT"].get(
            "docker_compose_command", DEFAULT_DOCKER_COMPOSE_COMMAND
        )  # type: ignore

    @property
    def db_path(self) -> Path:
        return self._db_path

    @db_path.setter
    def db_path(self, value: str) -> None:
        self._db_path = Path(value).expanduser()

    @property
    def docker_compose_command(self) -> list[str]:
        return self._docker_compose_command

    @docker_compose_command.setter
    def docker_compose_command(self, value: str | list[str]) -> None:
        if isinstance(value, list):
            self._docker_compose_command = value
        else:
            self._docker_compose_command = value.split()


Config = DCWConfig()
