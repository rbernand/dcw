class DCWException(Exception):
    msg = "undefined errpr."


class DuplicateProjectName(DCWException):
    msg = "A project already exists with this name"


class ProjectNotFound(DCWException):
    msg = "ProjectNotFound"


class NoComposeFile(DCWException):
    msg = "No config file found."
