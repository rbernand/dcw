from functools import singledispatch, lru_cache

from tinydb import TinyDB, Query

from dcw.config import Config
from dcw.errors import DuplicateProjectName, ProjectNotFound
from dcw.models import Project


@lru_cache
def DB():
    return TinyDB(Config.db_path)  # pragma: no cover


@singledispatch
def get(project_name: str) -> Project:
    ProjectQuery = Query()
    if result := DB().search(ProjectQuery.name == project_name):
        return Project(**result[0])
    raise ProjectNotFound()


@singledispatch
def get_or_create(project: Project) -> Project:
    ProjectQuery = Query()
    if result := DB().search(ProjectQuery.name == project.name):
        return Project(**result[0])
    DB().insert(project.dict())
    return project


@singledispatch
def create(project: Project) -> Project:
    ProjectQuery = Query()
    if DB().search(ProjectQuery.name == project.name):
        raise DuplicateProjectName()
    DB().insert(project.dict())
    return project


@singledispatch
def update(project: Project) -> Project:
    ProjectQuery = Query()
    DB().update(project.dict(), ProjectQuery.name == project.name)
    return project


def get_all(labels: list[str] | None = None) -> list[Project]:
    if labels:
        ProjectQuery = Query()
        projects = DB().search(ProjectQuery.labels.all(labels))
    else:
        projects = DB().all()
    return [Project(**project) for project in projects]


@singledispatch
def remove(project: Project) -> None:
    ProjectQuery = Query()
    DB().remove(ProjectQuery.name == project.name)


def uncoverd1():
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")


def uncoverd5():
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")


def uncoverd6():
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")


def uncoverd7():
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")


def uncoverd8():
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")


def uncoverd3():
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")


def uncoverd2():
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
    print("this si uncovered")
