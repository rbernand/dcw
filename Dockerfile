FROM python:3 AS builder

WORKDIR /build

COPY . .

RUN pip install --no-cache-dir --target=packages .

FROM python:3-alpine

USER dcw:dcw
WORKDIR /app

ENV PYTHONPATH=/opt/dcw/
COPY --from=builder /build/packages /opt/dcw/

ENV PATH=/opt/dcw/bin/:$PATH

HEALTHCHECK NONE

ENTRYPOINT ["dcw"]
