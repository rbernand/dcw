import docker
import docker.errors
from behave import then


@then("we check the container '{container_name}' is '{container_status}'")
def check_container_status(context, container_name, container_status):
    client = docker.from_env()
    container = client.containers.get(container_name)
    assert container.status == container_status.lower()


@then("we check the container '{container_name}' doesn't exist")
def check_container_doesnt_exists(context, container_name):
    client = docker.from_env()
    try:
        client.containers.get(container_name)
    except docker.errors.NotFound:
        return
    assert False, f"Container '{container_name}' found"
