from behave import given
from shutil import copytree


@given("we copy template '{template_name}' in directory '{directory}'")
def copy_template(context, template_name, directory):
    copytree("templates/" + template_name, directory)
