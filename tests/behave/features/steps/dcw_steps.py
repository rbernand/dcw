from behave import when
import subprocess


@when("we run dcw command '{dcw_command}'")
def dcw_run_command(context, dcw_command):
    command = dcw_command.split()
    ret = subprocess.run(command, shell=False, capture_output=True)
    assert ret.returncode == 0, ret.stderr
