Feature: Start a docker compose stack

  Scenario: Start a compose.yml stack
      When we run dcw command 'dcw add template1 ./templates/template1'
       And we run dcw command 'dcw in template1 up -d'

      Then we check the container 'template1-service' is 'running'

  Scenario: Stop a compose.yml stack
      When we run dcw command 'dcw in template1 stop'

      Then we check the container 'template1-service' is 'exited'

  Scenario: Down a compose.yml
      When we run dcw command 'dcw in template1 down'

      Then we check the container 'template1-service' doesn't exist

  Scenario: Remove project from DCW
      When we run dcw command 'dcw rm template1'
