import pathlib


def create_dcw_config():
    with pathlib.Path("~/.dcwrc").expanduser().open("w") as stream:
        stream.write("[DEFAULT]\n")
        stream.write("db_path = ~/dcw.db\n")
        stream.write("docker_compose_command = docker compose\n")


def before_all(context):
    create_dcw_config()
