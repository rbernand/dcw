import pytest

from dcw.commands.list_command import ListCommand


class Args: ...


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.list_command.db")
    yield mock_db


def test_execute__should_list_no_project(mock_db, capsys):
    mock_db.get_all.return_value = []
    ListCommand()._execute(Args(), [])

    out, _ = capsys.readouterr()
    assert out == "No projects.\n"


def test_execute__should_list_projects(mock_db, capsys, mocker):
    project_a = mocker.MagicMock()
    project_b = mocker.MagicMock()
    project_a.name = "a"
    project_a.path = "/a"
    project_b.name = "b"
    project_b.path = "/b"

    mock_db.get_all.return_value = [project_a, project_b]

    ListCommand()._execute(Args(), [])
    out, _ = capsys.readouterr()

    assert (
        out
        == """a: /a
b: /b
"""
    )
