import pytest

from dcw.commands.in_command import InCommand


class Args:
    def __init__(self, project_name):
        self.project_name = project_name


@pytest.fixture(autouse=True)
def config_test(monkeypatch):
    monkeypatch.setattr("dcw.config.Config._docker_compose_command", ["marcel"])


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.in_command.db")
    yield mock_db


def test_execute__nominal(mocker, mock_db):
    mock_project = mocker.MagicMock()
    mock_project.name = "foo"
    mock_project.path = "/tmp"
    mock_project.prepare_command.return_value = ["marcel", "a", "b"]
    mock_db.get.return_value = mock_project
    mock_os = mocker.patch("dcw.commands.in_command.os")
    mock_subprocess = mocker.patch("dcw.commands.in_command.subprocess")
    mock_subprocess.run.return_value.returncode = 1337
    mock_sys = mocker.patch("dcw.commands.in_command.sys")

    InCommand()._execute(Args(project_name="foo"), ["a", "b"])

    mock_sys.exit.assert_called_once_with(1337)
    mock_subprocess.run.assert_called_once_with(["marcel", "a", "b"], shell=False)
    mock_os.chdir.assert_called_once_with("/tmp")


def test_execute__should_raise_for_project_not_found(mocker):
    pass
