import pytest

from dcw.commands.add_command import AddCommand


@pytest.fixture
def mock_project(mocker):
    mock_project = mocker.patch("dcw.commands.add_command.Project")
    yield mock_project


class Args:
    def __init__(
        self,
        project_name="foo",
        project_path="/tmp",
        labels=None,
        compose_file=None,
        force=False,
    ):
        self.project_name = project_name
        self.project_path = project_path
        self.labels = labels or []
        self.compose_file = compose_file or []
        self.force = force


@pytest.fixture(autouse=True)
def config_test(monkeypatch):
    monkeypatch.setattr("dcw.config.Config._docker_compose_command", ["marcel"])


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.add_command.db")
    yield mock_db


def test_execute__should_add_a_project_and_check_if_compose_file_exists(
    mock_project, mock_db
):
    mock_project.check.return_value = None
    mock_project.name = "bar"
    mock_project.path = "/baz"
    mock_db.create.return_value = mock_project

    AddCommand()._execute(Args("bar", "/baz", force=False), [])

    mock_db.create.assert_called_once()
    mock_project.return_value.check.assert_called_once()


def test_execute__should_add_a_project_and_not_check_if_force(mock_project, mock_db):
    mock_project.check.return_value = None
    mock_project.name = "bar"
    mock_project.path = "/baz"
    mock_db.create.return_value = mock_project

    AddCommand()._execute(Args("bar", "/baz", force=True), [])

    mock_db.create.assert_called_once()
    assert not mock_project.return_value.check.called


def test_execute__should_create_project_with_labels(mock_project, mock_db):
    mock_project.check.return_value = None
    mock_project.name = "bar"
    mock_project.path = "/baz"

    AddCommand()._execute(
        Args(
            project_name="bar", project_path="/baz", labels=["foo", "bar"], force=True
        ),
        [],
    )
    mock_project.assert_called_once_with(
        name="bar", path="/baz", labels=["foo", "bar"], compose_files=[]
    )


def test_execute__should_create_project_with_compose_files(mock_project, mock_db):
    mock_project.check.return_value = None
    mock_project.name = "bar"
    mock_project.path = "/baz"

    AddCommand()._execute(
        Args(
            project_name="bar",
            project_path="/baz",
            compose_file=["foo", "bar"],
            force=True,
        ),
        [],
    )
    mock_project.assert_called_once_with(
        name="bar", path="/baz", labels=set(), compose_files=["foo", "bar"]
    )
