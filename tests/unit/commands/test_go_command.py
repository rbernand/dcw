import pytest

from dcw.commands.go_command import GoCommand


@pytest.fixture
def mock_project(mocker):
    mock_project = mocker.patch("dcw.commands.go_command.Porject")
    yield mock_project


class Args:
    def __init__(self, project_name):
        self.project_name = project_name


@pytest.fixture(autouse=True)
def config_test(monkeypatch):
    monkeypatch.setattr("dcw.config.Config._docker_compose_command", ["marcel"])


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.go_command.db")
    yield mock_db


def test_execute__should_no_nothing_without_projects(monkeypatch, mocker, mock_db):
    mock_os = mocker.patch("dcw.commands.go_command.os")
    mock_os.environ.get.return_value = "shell"
    project = mocker.Mock()
    project.name = "foo"
    project.path = "/tmp"
    mock_db.get.return_value = project

    command = GoCommand()
    command._execute(Args(project_name="foo"), [])

    mock_os.chdir.assert_called_once_with("/tmp")
    mock_os.execl.assert_called_once_with("shell", "shell")
