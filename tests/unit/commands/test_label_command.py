import pytest

from dcw.commands.label_command import LabelCommand


class Args:
    def __init__(
        self,
        label_action,
        project_name="foo",
        labels=None,
    ):
        self.project_name = project_name
        self.label_action = label_action
        self.labels = labels or []


@pytest.fixture(autouse=True)
def config_test(monkeypatch):
    monkeypatch.setattr("dcw.config.Config._docker_compose_command", ["marcel"])


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.label_command.db")
    yield mock_db


def test_add_label__should_add_labels_to_project(mocker):
    project_labels = set()
    labels = ["a", "b", "c"]

    LabelCommand()._add_labels(project_labels, labels)
    assert project_labels == {"a", "b", "c"}


def test_remove_label__should_remove_labels():
    project_labels = {"a", "b", "c"}
    labels = ["a", "b"]

    LabelCommand()._remove_labels(project_labels, labels)

    assert project_labels == {"c"}


def test_remove_label__should_remove_labels_and_print_if_not_found(capsys):
    project_labels = {"a", "b"}
    labels = ["a", "c"]

    LabelCommand()._remove_labels(project_labels, labels)
    _, err = capsys.readouterr()

    assert err == "Label 'c' not found.\n"
    assert project_labels == {"b"}


def test_execute__should_call_add_label(mocker, mock_db):
    project = mocker.MagicMock()
    mock_add_labels = mocker.patch(
        "dcw.commands.label_command.LabelCommand._add_labels"
    )
    mock_remove_labels = mocker.patch(
        "dcw.commands.label_command.LabelCommand._remove_labels"
    )
    mock_db.get.return_value = project

    LabelCommand()._execute(
        Args(project_name="foo", label_action="add", labels=["a", "b"]), []
    )

    mock_db.get.assert_called_once_with("foo")
    mock_add_labels.assert_called_once_with(project.labels, ["a", "b"])
    assert not mock_remove_labels.called
