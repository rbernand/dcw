import pytest

from dcw.commands.config_command import ConfigCommand


class Args: ...


@pytest.fixture(autouse=True)
def config_test(monkeypatch):
    monkeypatch.setattr("dcw.config.Config._docker_compose_command", ["marcel"])
    monkeypatch.setattr("dcw.config.Config._db_path", "/here")


def test_execute__should_add_a_project_and_check_if_compose_file_exists(capsys):
    ConfigCommand()._execute(Args(), [])
    out, _ = capsys.readouterr()
    assert (
        out
        == """db_path: /here
docker_compose_command: marcel
"""
    )
