import pytest

from dcw.commands.all_command import AllCommand


class Project:
    def __init__(self, name, path, labels=None):
        self.name = name
        self.path = path
        self.labels = labels or {}


class Args:
    def __init__(self, labels=None):
        self.labels = labels or {}


@pytest.fixture(autouse=True)
def config_test(monkeypatch):
    monkeypatch.setattr("dcw.config.Config._docker_compose_command", ["marcel"])


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.all_command.db")
    yield mock_db


def test_execute__should_no_nothing_without_projects(mocker, mock_db):
    command = AllCommand()

    mock_db.get_all = mocker.MagicMock(return_value={})
    mock_subprocess = mocker.patch("dcw.commands.all_command.subprocess.run")
    mock_os_chdir = mocker.patch("dcw.commands.all_command.os.chdir")

    command._execute(Args(), [])

    assert not mock_subprocess.called
    assert not mock_os_chdir.called


def test_execute__should_perform_command(mocker, mock_db):
    command = AllCommand()

    mock_db.get_all = mocker.MagicMock(side_effect=[[Project("foo", "bar")]])
    mock_subprocess = mocker.patch("dcw.commands.all_command.subprocess.run")
    mock_os_chdir = mocker.patch("dcw.commands.all_command.os.chdir")

    command._execute(Args(), ["foo"])

    mock_os_chdir.assert_called_once_with("bar")
    mock_subprocess.assert_called_once_with(["marcel", "foo"])
