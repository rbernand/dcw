import pytest

from dcw.commands.rm_command import RmCommand


class Args:
    def __init__(
        self,
        project_name="foo",
        project_path="/tmp",
        labels=None,
        compose_file=None,
        force=False,
    ):
        self.project_name = project_name
        self.project_path = project_path
        self.labels = labels or []
        self.compose_file = compose_file or []
        self.force = force


@pytest.fixture(autouse=True)
def config_test(monkeypatch):
    monkeypatch.setattr("dcw.config.Config._docker_compose_command", ["marcel"])


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.rm_command.db")
    yield mock_db


def test_execute__should_remove_an_existing_project(mocker, mock_db):
    project = mocker.MagicMock()
    mock_db.get.return_value = project

    RmCommand()._execute(Args(project_name="foo"), [])

    mock_db.get.assert_called_once_with("foo")
    mock_db.remove.assert_called_once_with(project)
