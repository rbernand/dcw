from dcw.commands.base_command import BaseCommand


def test_init__should_return_parser(mocker):
    ret = BaseCommand.init()

    import argparse

    assert isinstance(ret, argparse.ArgumentParser)
