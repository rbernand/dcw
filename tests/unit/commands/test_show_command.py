import pytest

from dcw.commands.show_command import ShowCommand


class Args:
    def __init__(self, project_name):
        self.project_name = project_name


@pytest.fixture(autouse=True)
def mock_db(mocker):
    mock_db = mocker.patch("dcw.commands.show_command.db")
    yield mock_db


def test_execute__should_print_project(mock_db, mocker):
    project = mocker.MagicMock()
    project.name = "foo"
    mock_db.get.return_value = project

    ShowCommand()._execute(Args(project_name="foo"), [])

    project.print.assert_called_once_with()
