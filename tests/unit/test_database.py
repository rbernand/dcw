import tinydb
from pathlib import Path
import pytest

from dcw import database, errors
from dcw.models import Project


@pytest.fixture
def project_foo():
    yield Project("foo", "/tmp", ["a_compose_file.yml"], ["a_label"])


@pytest.fixture
def project_bar():
    yield Project("bar", "/tmp/bar", [], [])


@pytest.fixture(scope="function", autouse=True)
def populate(mocker, tmp_path, project_foo, project_bar):
    db_path = tmp_path / "tmpdb.json"
    database = tinydb.TinyDB(db_path)
    mock_db = mocker.patch("dcw.database.DB", return_value=database)
    database.insert(project_foo.dict())
    database.insert(project_bar.dict())
    yield mock_db


def test_get_db_should_return_the_same_instance(mocker):
    mock_tinydb = mocker.patch("dcw.database.TinyDB")

    ins1 = database.DB()
    ins2 = database.DB()

    assert ins1 is ins2
    mock_tinydb.assert_called_once


def test_get__should_retrieve_a_project_by_name():
    project = database.get("foo")
    assert project.name == "foo"
    assert project.path.as_posix() == "/tmp"
    assert project.compose_files == [Path("a_compose_file.yml")]
    assert project.labels == {"a_label"}


def test_get__should_raise_when_not_found():
    with pytest.raises(errors.ProjectNotFound):
        database.get("notfound")


def test_get_or_create__should_retrieve_a_project_by_name():
    project = Project("foo", "/tmp")
    project_result = database.get_or_create(project)
    assert project_result.name == "foo"
    assert project_result.path.as_posix() == "/tmp"
    assert project_result.compose_files == [Path("a_compose_file.yml")]
    assert project_result.labels == {"a_label"}
    # TODO: don't test with empty list/set, retrieves them


def test_get_or_create__should_create_a_project():
    project = Project("baz", "/tmp/baz")
    project_result = database.get_or_create(project)
    assert project_result.name == "baz"
    assert project_result.path.as_posix() == "/tmp/baz"
    assert project_result.compose_files == []
    assert project_result.labels == set()


def test_create__should_create_a_project():
    project = Project("baz", "/tmp/baz")
    project_result = database.create(project)
    assert project_result.name == "baz"
    assert project_result.path.as_posix() == "/tmp/baz"
    assert project_result.compose_files == []
    assert project_result.labels == set()


def test_create__should_raise_when_project_already_exists():
    project = Project("foo", "/tmp/")
    with pytest.raises(errors.DuplicateProjectName):
        database.create(project)


def test_update__should_update_any_field():
    project = database.get("foo")
    project.path = "/tmp/another"
    project.compose_files = ["/tmp/another/anything.yml"]
    project.labels = {"a", "b", "c"}
    database.update(project)
    project = database.get("foo")
    assert project.path == Path("/tmp/another")
    assert project.compose_files == [Path("/tmp/another/anything.yml")]
    assert project.labels == {"a", "b", "c"}


def test_get_all__should_return_all_projects():
    projects = database.get_all()
    assert projects[0].name == "foo"
    assert projects[1].name == "bar"
    assert len(projects) == 2


def test_get_all_should_filter_by_labels():
    projects = database.get_all(labels=["a_label"])
    assert projects[0].name == "foo"
    assert len(projects) == 1


def test_remote__should_delete_a_project():
    project = Project("foo", "/tmp/foo")
    database.remove(project)
    with pytest.raises(errors.ProjectNotFound):
        database.get(project.name)
