from pathlib import Path
import pytest

from dcw import errors
from dcw.models import Project


def test_project__nominal_minimal():
    project = Project("foo", "/tmp")
    assert project.name == "foo"
    assert isinstance(project.path, Path)
    assert project.path.as_posix() == "/tmp"


def test_project__nominal_with_compose_files_with_str_value():
    project = Project("foo", "/tmp", compose_files=["a", "b"])
    assert project.name == "foo"
    assert isinstance(project.path, Path)
    assert project.path.as_posix() == "/tmp"
    assert isinstance(project.compose_files, list)
    assert len(project.compose_files) == 2
    assert isinstance(project.compose_files[0], Path)
    assert isinstance(project.compose_files[1], Path)


def test_project__nominal_with_compose_files_with_path_value():
    project = Project("foo", Path("/tmp"), compose_files=[Path("a"), Path("b")])
    assert project.name == "foo"
    assert isinstance(project.path, Path)
    assert project.path == Path("/tmp")
    assert isinstance(project.compose_files, list)
    assert len(project.compose_files) == 2
    assert isinstance(project.compose_files[0], Path)
    assert isinstance(project.compose_files[1], Path)


def test_project__nominal_with_labels():
    project = Project("foo", "/tmp", labels={"bar", "baz"})
    assert project.name == "foo"
    assert isinstance(project.path, Path)
    assert project.path.as_posix() == "/tmp"
    assert isinstance(project.labels, set)
    assert isinstance(project.labels.pop(), str)
    assert isinstance(project.labels.pop(), str)


def test_project_should_raise_with_invalid_type_for_compose_files():
    with pytest.raises(TypeError):
        Project("foo", 2)


def test_project__dict():
    project = Project("foo", "/tmp", compose_files=["a", "b"], labels=["bar", "baz"])
    project_dict = project.dict()
    assert sorted(project_dict["labels"]) == sorted(["bar", "baz"])
    assert project_dict["name"] == "foo"
    assert project_dict["path"] == "/tmp"
    assert project_dict["compose_files"] == ["a", "b"]


def test_project__check(mocker):
    mock_find_default_compose_file = mocker.patch(
        "dcw.models.Project._find_default_compose_file"
    )
    project = Project("foo", "/tmp")
    project.check()
    mock_find_default_compose_file.assert_called_once_with()


def test_project__prepare_command_should_format_shell_command(monkeypatch):
    project = Project("foo", "/tmp")
    monkeypatch.setattr("dcw.models.Config.docker_compose_command", "marcel do")
    assert project.prepare_command(["some", "stuff"]) == [
        "marcel",
        "do",
        "some",
        "stuff",
    ]


def test_project__prepare_command_should_format_shell_command_with_compose_files(
    monkeypatch,
):
    project = Project("foo", "/tmp", ["a", "b"])
    monkeypatch.setattr("dcw.models.Config.docker_compose_command", "marcel do")
    assert project.prepare_command(["some", "stuff"]) == [
        "marcel",
        "do",
        "-f",
        "a",
        "-f",
        "b",
        "some",
        "stuff",
    ]


def test_find_default_compose_file__should_find_a_file(tmp_path):
    compose_file_path = tmp_path / "compose.yml"
    compose_file_path.touch()
    project = Project("foo", tmp_path)
    result = project._find_default_compose_file()
    assert result == compose_file_path


def test_find_default_compose_file__should_raise_when_no_compose_file_found(tmp_path):
    project = Project("foo", tmp_path)
    with pytest.raises(errors.NoComposeFile):
        project._find_default_compose_file()


def test_print__should_print_intormations(capsys):
    project = Project("foo", "/tmp", ["foo.yml"], {"label"})
    project.print()
    captured_out, err = capsys.readouterr()
    assert (
        captured_out
        == """Name: foo
Path: /tmp
Compose files:
    - foo.yml
Labels:
    - label
"""
    )
