from dcw.__main__ import main
from dcw.errors import DCWException


def test_main__nominal(mocker):
    mock_base_commands = mocker.patch("dcw.__main__.BaseCommand")
    mock_base_commands.init.return_value.parse_known_args.return_value = ("foo", "bar")

    main()

    mock_base_commands.init.assert_called_once()
    mock_base_commands.init.return_value.parse_known_args.assert_called_once()
    mock_base_commands.execute.assert_called_once_with("foo", "bar")


def test_main__should_exit_property_in_case_of_error(mocker):
    mock_sys = mocker.patch("dcw.__main__.sys")
    mock_base_commands = mocker.patch("dcw.__main__.BaseCommand")
    mock_base_commands.init.return_value.parse_known_args.return_value = ("foo", "bar")
    mock_base_commands.execute.side_effect = DCWException

    main()

    mock_base_commands.init.assert_called_once()
    mock_base_commands.init.return_value.parse_known_args.assert_called_once()
    mock_base_commands.execute.assert_called_once_with("foo", "bar")
    mock_sys.exit.assert_called_once_with(1)
