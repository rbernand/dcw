import pytest


@pytest.fixture(autouse=True)
def custom_config(monkeypatch, tmp_path):
    config_file = tmp_path / "config.ini"
    tmp_db = tmp_path / "db.json"
    config_file.write_text(
        f"""
[DEFAULT]
db_path = {tmp_db}
docker_compose_command = marcel
"""
    )
    monkeypatch.setattr("dcw.config.CONFIG_FILES", [config_file])
    monkeypatch.setattr("dcw.config.Config.docker_compose_command", "/tmp/db.json")
    monkeypatch.setattr("dcw.config.Config.docker_compose_command", "marcel")
    yield
